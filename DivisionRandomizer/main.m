//
//  main.m
//  DivisionRandomizer
//
//  Created by Joseph Canale on 9/4/17.
//  Copyright © 2017 Joseph Canale. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *teams = @[@"Jack", @"Joe", @"Nunzio", @"Trevor", @"Mac", @"Jim", @"Nick", @"Mick", @"Dieck", @"Matt"];
        
        NSMutableArray *divisionOne = [[NSMutableArray alloc] init];
        NSMutableArray *divisionTwo = [[NSMutableArray alloc] init];
        
        for (NSString *team in teams) {
            float randomNumber = arc4random_uniform(2);
            
            if (randomNumber == 0) {
                if (divisionOne.count >= 5 ) {
                    [divisionTwo addObject:team];
                } else {
                    [divisionOne addObject:team];
                }
            } else if (randomNumber == 1) {
                if (divisionTwo.count >= 5 ) {
                    [divisionOne addObject:team];
                } else {
                    [divisionTwo addObject:team];
                }
            }
        }
        
        NSLog(@"Divison One Teams:");
        
        for (NSString *team in divisionOne) {
            NSLog(@"%@", team);
        }
        
        NSLog(@"Divison Two Teams:");
        
        for (NSString *team in divisionTwo) {
            NSLog(@"%@", team);
        }
        
    }
    return 0;
}
